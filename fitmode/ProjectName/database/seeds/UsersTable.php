<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder{
    public function run()    
    {        
        $admin = DB::table('users');        
        if ($admin->count() == 0) {           
             // Create new admin            
             $admin->insert([                
                 'name' => env('ADMIN_USER_NAME', 'Admimistration'),                
                 'email' => env('ADMIN_EMAIL', 'example@mail.com'),   
                 'password' => env('ADMIN_PASSWORD', '12345678'),  
                 'status' => 1,  
                 'code' => '123456'           
                 ]);        
                }    
    }

}

