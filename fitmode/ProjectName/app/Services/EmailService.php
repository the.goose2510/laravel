<?php
namespace App\Services;
use Log;
use Illuminate\Support\Facades\Mail;


class EmailService {
    public function sendText($to, $subject, $body) 
    {        // Check input params        
        if (!isset($to) || !isset($subject) || !isset($body)) 
        {            
            Log::error('Sending Email (Text) Error: Invalid input params');
                        return ['result' => false, 'error_msg' => 'Invalid input params'];        
        }        
        Log::info('Sending Email (Text) Start: to: '.$to.', subject: '.$subject.', body: '.$body);        
        try {            // Send email            
                $this->mail::send([], [], function($message) use ($to, $subject, $body) 
            {                
                $message                    
                ->to($to)                    
                ->subject($subject)                    
                ->setBody($body);            
            });            
                Log::info('Sending Email (Text) Success');            
                return ['result' => true, 'error_msg' => ''];        
            } catch (\Exception $e) 
            {            
                Log::error('Sending Email (Text) Error: Error message: '.$e->getMessage());            
                return ['result' => false, 'error_msg' => $e->getMessage()];        
            }    
    }
}
