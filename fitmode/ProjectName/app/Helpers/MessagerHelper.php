<?php

const _MESSAGES = [    // Common messages    
    1001 => 'fail'        // Default message
];
const _ERRORS = [
    1000 => "fail"
];

function get_message($code, $default_message = '') {    // Return normal message    
    if (isset(_MESSAGES[$code])) 
    {        
        return _MESSAGES[$code];    
    }    
    // Return error message    
    else if (isset(_ERRORS[$code])) 
    {        
        return _ERRORS[$code];    
    }    
    // Return default message    
    else 
    {        
        return $default_message;    
    }
}