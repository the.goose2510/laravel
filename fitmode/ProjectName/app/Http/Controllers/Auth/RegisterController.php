<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\Model\Customers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    // protected function create(array $data)
    // {
    //     return User::create([
    //         'name' => $data['name'],
    //         'email' => $data['email'],
    //         'password' => Hash::make($data['password']),
    //     ]);
    // }
    //signup
    public function sigup(Request $request)
    {
        $rule =[
            'username' => 'required',
            'password' => 'required',
            'email' => 'required'
        ];
        $message = [
            'username.required' =>[
                'error_code' => 9001,
                'message' => get_message(9001,''),
            ],            
            'password.required' =>[
                'error_code' => 9002,
                'message' => get_message(9002,'')
            ],
            'email.required' =>[
                'error_code' => 9003,
                'message' => get_message(9003,'')
            ]            
        ];
        $validation = Validator::make($request -> all(), $rule, $message);
        $error_array = array();
        $success_output = '';
        if ($validation->fails())
        {
            foreach($validation->messages()->getMessages() as $field_name => $messages)
            {
                $respone['error'][] = $messages;
            }        
        }
        else 
        {
            $user = $this->customer->getUser($request->get('username'),$request->get('password'));
            if(null == $user){
                DB::transation();
                try{
                    $user [] = [
                        'username' => $request->get('username'),
                        'password' => $request->get('username'),
                        'email'    => $request->get('username'),
                        'status'   => $request->get('status')
                    ];
     
                    $user_id = $this->customer->_save($user);
                    if(null== $user_id){
                        DB::rollback();
                    }

                DB::commit();

                $respone['OK']=[
                    "result" => "1",
                    "message" => "ok",
                    "item" => json_decode($user)
                ];

                }catch(\Exception $e){
                    DB::rollback();
                }
            }
            else
            {
                $respone['OK']=[
                    "result" => "1",
                    "message" => "ok",
                    "item" => json_decode($user)
                ];
            }
        }
    }
}
