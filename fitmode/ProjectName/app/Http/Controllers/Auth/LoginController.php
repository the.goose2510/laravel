<?php

// namespace App\Http\Controllers\Auth;
// // use App\Http\Controllers\Controller;
// use Illuminate\Foundation\Auth\AuthenticatesUsers;
// use Illuminate\Http\Request;
// use App\Model\Customers;


namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Validator;
// use App\Helpers;
use App\Models\Customer;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home',$customer;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Customer $customer)
    {
        $this->middleware('guest')->except('logout');
        $this->customer= $customer;
    }


    //signin
    public function sigin(Request $request)
    {
        $rule =[
            'username' => 'required',
            'password' => 'required'
        ];
        $message = [
            'username.required' =>[
                'error_code' => 9002,
                'message' => get_message(9002,'')
            ],            
            'password.required' =>[
            
                'error_code' => 9002,
                'message' => get_message(9002,'')
            ]           
        ];
        $validation = Validator::make($request -> all(), $rule, $message);
        $response_ok=[];
        $response_fail=[];
        if ($validation->fails())
        {
            foreach($validation->messages()->getMessages() as $field_name => $messages)
            {
                $response_fail[] = $messages;
            }        
        }else 
        {
            $user = $this->customer->getUser($request->get('username'),$request->get('password'));
            if(null == $user){
                $response_fail[]=[
                    "result" => "0",
                    "message" => "fail"
                ];
                return json_encode($respone);
            }
            else
            {
                $response_ok[]=[
                    "result" => "1",
                    "message" => "ok",
                    "item" => json_decode($user)
                ];
                $response_ok[]=[
                    "item1" => json_decode($user)
                ];
            }
        }
        $output= array(
            'error'=> $response_fail,
            'success' =>$response_ok
        );
        echo json_encode($output);
    }
}

    
