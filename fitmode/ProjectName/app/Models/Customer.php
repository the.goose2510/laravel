<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = "users";

    public function getUser($username,$password){
        return $this->where("username", $username)
                    ->where("password", $password)    
                    ->get();
    }

    public function _save($recipient) {        
        if (!isset($recipient)) {            
            return null;        
        }        
        $updated_recipient = $this->updateOrCreate(           
                [ 'id' => isset($recipient['id']) ? $recipient['id'] : null ],            
                $recipient        
            );        
    return isset($updated_recipient) ? $updated_recipient->id : null;    
    }
}
